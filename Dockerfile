FROM openjdk:17-jdk-slim


ADD target/springboot-gitlabcicd-0.0.1-SNAPSHOT.jar springboot-gitlabcicd.jar

ENTRYPOINT ["java", "-jar", "springboot-gitlabcicd.jar"]