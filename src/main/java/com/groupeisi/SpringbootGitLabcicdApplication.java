package com.groupeisi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootGitLabcicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootGitLabcicdApplication.class, args);
	}

}
